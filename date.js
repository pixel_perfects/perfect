// Unix time -> 01.01.1970 -> 0ms

// Date();

// console.log(date.getDay());

// getHours() - часы
// getMinutes() - минуты
// getSeconds() - секунды

// вывести на консоль текущее время в формате HH:mm:ss (19:23:45)
// hours + ':' + minutes + ':' , concat, `${hours} : ${minutes} : ${secons}`

function getCurrentTime() {
    const date = new Date();

    const hours = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();

    return `${addZero(hours)}:${addZero(minutes)}:${addZero(seconds)}`; 
}

function addZero(value) {
    /* if (value.toString().length === 1) {
        return `0${value}`;
    }
    return value; */

    return value.length === 1 ? `0${value}` : value;
}

console.log(getCurrentTime());

const myDate = new Date();

myDate.setHours(4);


console.log(myDate.getTime());
// moment js

moment.locale('ru');
const time = moment('12-01-2019', 'DD-MM-YYYY').format('LL');// .format('DD/MM/YY');
console.log(time);